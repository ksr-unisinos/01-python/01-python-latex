# 01-python-latex

Este projeto utiliza a biblioteca `pytexit` e implementa nela algumas funcionalidades para facilitar a inserção no LaTeX.

será necessário instalar esta biblioteca

```pip install pytexit```

também foi utilizado nesse exemplo a biblioteca `numpy`

```pip install numpy```

Já a biblioteca `printSI` foi criada por este autor e não é necessário instala-la, basta deixar na mesma pasta do projeto.

Para teste das equações, foi utilizado um exemplo deste autor no ```overleaf```

https://www.overleaf.com/read/bpnrhprzmdbn

https://gitlab.com/viniciusglinden/latex-python

https://www.ctan.org/pkg/circuitikz
