from printSI import printSI
from numpy import sqrt, pi

I = 5e-3
V = 10
t_o = 15
t_i = 152

latex = ''

R = V/I
latex += printSI(R,'R','k','Ω',latexEQU = 'R = V/I')

P = R*I**2
latex += printSI(P,'P','','W',latexEQU = 'P = R*I**2')

P = V**2/R
latex += printSI(P,'P','m','W',latexEQU = 'P = V**2/R')

V = sqrt(P*R)
latex += printSI(V,'V','m','V',latexEQU = 'V = sqrt(P*R)')

I = sqrt(P/R)
latex += printSI(I,'I','m','A',latexEQU = 'I = sqrt(P/R)')


Δt = t_o - t_i
latex += printSI(Δt,'Δt','','s',latexEQU = 'Δt = t_o - t_i')

π = pi
latex += printSI(π,'π','','',latexEQU = 'π = pi ')


print (latex)